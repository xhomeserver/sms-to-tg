# SMS to Telegram gateway

Simple SMS to Telegram gateway built with an USB dongle 3G modem Huawei e1550 or similar connected to Linux PC.

The example code was tested on
- Virtual Machine with freshly installed Ubuntu Server 20.04
- OrangePi Zero with Armbian 20.08.14 Focal OS

## environment and settings
```
sudo apt-get install gammu-smsd
```

Here is the fix for Gammu-smsd version 1.41.0 on Ubuntu 20.  
Apply in case you have got an error at a service starting stage of installation
```
sudo sed -i 's|${CMAKE_INSTALL_FULL_BINDIR}|/usr/bin|g' /lib/systemd/system/gammu-smsd.service
sudo systemctl daemon-reload
sudo systemctl restart gammu-smsd
```
Create a directory structure for the daemon
```
sudo mkdir -p /var/log/gammu /var/spool/gammu/{inbox,outbox,sent,error}
```

**gammu-smsd config**  
Change the port in the [gammu] section to your device.  
Add the RunOnReceive in the [smsd] section.  
Check my example in [gamu_config](https://gitlab.com/xhomeserver/sms-to-tg/-/tree/master/gammu_config).  

```
sudo nano /etc/gammu-smsdrc
```

Download this repository to the folder you are going to run the scripts.
In my case it is a user home directory
```
mkdir ~/pyapps
cd ~/pyapps
git clone https://gitlab.com/xhomeserver/sms-to-tg.git
cd ./sms-to-tg
```

Environment configuration **env.sh** file with your Telegram Bot credentials should be created in the project directory.  
Here is the example (provide your token and chat id)
````
#!/bin/bash
export TG_TOKEN='1214098089:ADGMev8VUTvD1SKLDJSLFFMEiOyc-OirhLNA'
export TG_CHATID='997341773'
````


## additional info and "inspired by" sources
- https://wammu.eu/docs/manual/smsd/run.html
- https://wammu.eu/docs/manual/smsd/pgsql.html
- https://jonnev.se/sms-gateway-and-push-notifications/

## todo
**1) Symlink to /dev/sms for consistency**

It might be a good idea to create a symlink for your device so your settings for gammu always works, otherwise sometimes after reboot it might switch to /dev/ttyUSB1 or whatever.
```
sudo nano /etc/udev/rules.d/999-sms-gateway.rules
# Add this line 
SUBSYSTEM=="tty", ATTRS{idVendor}=="12d1", ATTRS{idProduct}=="1506", SYMLINK+="sms"
# Then
sudo udevadm control --reload
```

**2) Add more security**

gammu-smsd has been running under the root user in my setup. Whereas python scripts it triggers can be changed by non-priveleged user. This should be changed in "production".

