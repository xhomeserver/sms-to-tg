import requests


class Bot:
    BASE_URL = 'https://api.telegram.org'

    def __init__(self, token):
        self.token = token
        self.base_url = f'{self.BASE_URL}/bot{token}'

    def send_message(
        self,
        chat_id,
        text,
        parse_mode='HTML',
        disable_notification=True,
    ):
        payload = {
            'chat_id': chat_id,
            'text': text,
            'parse_mode': parse_mode,
            'disable_notification': disable_notification,
        }
        response = requests.post(
            f'{self.base_url}/sendMessage', json=payload, timeout=3
        )
        return response.json()
