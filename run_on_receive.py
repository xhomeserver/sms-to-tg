import os, sys, re
from tg_bot import Bot

TG_TOKEN = os.environ['TG_TOKEN']
TG_CHATID = os.environ['TG_CHATID']


def get_message():
    files = [os.path.join('/var/spool/gammu/inbox/', m) for m in sys.argv[1:]]
    files.sort()
    number = re.match(r'^IN\d+_\d+_\d+_(.*)_\d+\.txt', os.path.split(files[0])[1]).group(1)
    text = ''
    for f in files:
        text += open(f, 'r').read()

    return number, text


sms_number, sms_text = get_message()
tg_message = f'SMS from {sms_number}: {sms_text}'

tg_bot = Bot(token=TG_TOKEN)
tg_bot.send_message(chat_id=TG_CHATID, text=tg_message)
